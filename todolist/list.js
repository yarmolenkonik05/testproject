const todo_text = document.getElementById("todo_text")
const add_todo = document.getElementById("add_todo")
const todo_list = document.getElementById("todo_list")



const updateList = () => {
    todo_list.innerText = ''

    const todos = JSON.parse(localStorage.getItem('todo_list')) || []

    todos.forEach(element => {
        
        const li = document.createElement("li")

        const todo_item = document.createElement('div')
        todo_item.innerText = element.todo_text

        const mark_as_completed = document.createElement('input')
        mark_as_completed.type = "checkbox"

        if(element.isCompleted) {
            li.classList.add('completed_task')
            mark_as_completed.checked = true
        }


        todo_item.prepend(mark_as_completed)

        const todo_delete = document.createElement("div")
        const todo_delete_btn = document.createElement("button")

        todo_delete_btn.innerHTML = "-"
        todo_delete.append(todo_delete_btn)

        li.append(todo_item)
        li.append(todo_delete)
        todo_list.append(li)

        todo_delete_btn.addEventListener("click", () => {
            const index = todos.findIndex(todo => todo.todo_text === element.todo_text)
        
            // [1,2,3,4,5]
            // [1,2] + [4,5]
            const firstPart = todos.slice(0, index)
            const secondPart = todos.slice(index + 1)

            localStorage.setItem('todo_list', JSON.stringify([...firstPart, ...secondPart]))
            li.remove()
        })
    
        mark_as_completed.addEventListener("change", (event) => {
            const index = todos.findIndex(todo => todo.todo_text === element.todo_text)
            todos[index].isCompleted = event.target.checked
            localStorage.setItem('todo_list', JSON.stringify(todos))

            if(event.target.checked) {
                li.classList.add('completed_task')
            } else {
                li.classList.remove('completed_task')
            }
        })
    });

}

add_todo.addEventListener("click", () => {

    const todos = JSON.parse(localStorage.getItem('todo_list')) || []

    todos.push({
        todo_text: todo_text.value, 
        isCompleted:false
    })

    localStorage.setItem('todo_list', JSON.stringify(todos))

    updateList()
    
    todo_text.value = ''
})



updateList()
