const button = document.getElementById("button")
const score = document.getElementById("score")
const timer = document.getElementById("timer")
const list = document.getElementById("list")
const timerSeconds = 5
const scoresList = []

let currentSeconds, ifStarted, clickedTimes
let StopTime = null



const init = () => {
    currentSeconds = 0
    ifStarted = false
    score.innerText = 0
    clickedTimes = 0
    timer.innerText = `${Math.floor(timerSeconds / 60)}:${timerSeconds % 60} sec`
    renderList()
}

const createLi = (text) => {
    const li = document.createElement('li')
    li.innerText = text
    list.append(li)
}

const renderList = () => {
    list.innerText = ''

    if(scoresList.length === 0) {
        createLi('Start clicking')
        return;
    }

    scoresList.sort((a ,b) => b - a)
    scoresList.forEach(elem => createLi(elem))
}


button.addEventListener("click", () => {
    if(ifStarted === false) {
        const StopTime = setInterval(() => {
            currentSeconds++;

            if(currentSeconds > timerSeconds) {
                clearInterval(StopTime)
                scoresList.push(score.innerText)
                init()
                return;
            }

            timer.innerText = `${Math.floor((timerSeconds - currentSeconds) / 60)}:${(timerSeconds - currentSeconds) % 60} sec`
    
        }, 1000)
    }


    ifStarted = true
    
    clickedTimes++

    score.innerText = clickedTimes
})


init()
